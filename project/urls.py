"""
project URL Configuration
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from rest_framework.authtoken import views as token_views
from rest_framework import routers

from users.forms import RegisterForm
router = routers.DefaultRouter()

# The router urls are contained here, and added to the router that is passed in
import users.views
users.views.set_routes(router)


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    url(r'^admin/', include(admin.site.urls)),

    # Allows public registration
    url(r'^register/', CreateView.as_view(
            template_name='registration/register.html',
            form_class=RegisterForm,
            success_url='/success/'
    ), name='create-normal-user'),
    url(r"^success/$"
      , TemplateView.as_view(template_name="registration/success.html")),

    # change password, etc
    # login here overrides the included one below
    url(r'^accounts/', include('django.contrib.auth.urls')),

    url(r'^api/', include(router.urls, namespace="api")),

    # POST u/p to get token
    url(r'^api-token-auth/', token_views.obtain_auth_token),
    # Get token for self while logged in
    url(r'^api-token/', users.views.get_auth_token),
    # session login/logout, same effect as registration akak users login
    url(r'^api-auth/', include('rest_framework.urls',
      namespace='rest_framework')),
    # Set permissions for app models against user groups
    url(r'^init/', users.views.initialize_user_permissions_and_groups
      , name="initialize")
]
