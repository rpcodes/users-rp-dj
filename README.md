## users-rp-django


Quickstart code for django site that requires user auth from django.contrib.auth
and DRF API access.
You probably want to change ```templates/base.html```.

There are some unresolved issues that should be fixed before using in production; see ```README.todo```.

Batteries included for:

* Authentication - Also note that by virtue of django rest framework, bootstrap is used for the
login and logout pages. This is easily changed by changing ```templates/rest_framework/login_base.html```
* Permissions and Roles - sets up a basic example of Admin and Member roles on a site; see users/permissions.py for the code that parses
* Django Rest Framework - including basic serializers and DRF browsable admin view for users
* Token API access - DRF and auth are set up to provide token to users. Note that DRF does not implement token expiry, and neither does this example.
* Registration - basic templates in users/templates/registration ... set to allow public registration, but disabling the /registration/ area in urls will disable this (DRF )

Just don't forget to run ```./manage.py migrate```.


You can clone and rename the project info, or start a new project and copy in
the users app. Note that your root urls config will need to be updated to
accomodate.
