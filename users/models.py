from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


#
# Signals
#

# Add Token to user model
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

#
# Serializers
#

from django.contrib.auth.models import User, Group
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):

    date_joined = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S",
        required=False, read_only=True)
    last_login = serializers.ReadOnlyField()
    group_labels = serializers.StringRelatedField(source="groups",many=True
      ,read_only=True, required=False )
    id = serializers.ReadOnlyField()

    class Meta:
        model = User
        fields = ('id','username', 'last_name', 'first_name', 'email', 'groups',
            'password', 'date_joined', 'last_login', 'group_labels','is_active')

        # thanks to http://stackoverflow.com/questions/27468552/changing-serializer-fields-on-the-fly/#answer-27471503
        extra_kwargs = {
            'password': {
                'write_only': True,
            },
        }

#    def create(self, validated_data):
#        warn("Password " + validated_data['password'])
#        g_data = validated_data.pop('groups')
#        user = User.objects.create_user(**validated_data)
#        user.groups = g_data
#        user.save()
#        return user


class GroupSerializer(serializers.ModelSerializer):
    group_id = serializers.ReadOnlyField(source='id')
    class Meta:
        model = Group
        fields = ('name', 'group_id')
