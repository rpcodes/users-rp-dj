from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import renderers
from .models import UserSerializer, GroupSerializer
from rest_framework.permissions import IsAdminUser
from django.contrib.auth.hashers import make_password
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework import generics
from rest_framework.decorators import detail_route
from logging import warn
from rest_framework.response import Response
from pdb import set_trace
from django.http import HttpResponse, JsonResponse
from rest_framework.authtoken.models import Token
from .permissions import ImprovedDjangoModelPermissions
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from .permissions import parse_app_permissions
import logging
logger = logging.getLogger(__name__)
logger.debug("imported {0}".format(__name__))

class ProtectedView(TemplateView):
    """
    Let's use easily use the login decorator for TemplateView
    """
    template_name = 'index.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedView, self).dispatch(*args, **kwargs)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    # Using a form we can still make a user, thanks to RegisterForm
    permission_classes = (ImprovedDjangoModelPermissions,)

    def perform_create(self, serializer):
        password = make_password(self.request.data['password'])
        serializer.save(password=password)

    def perform_update(self, serializer):
        password = make_password(self.request.data['password'])
        serializer.save(password=password)

    @detail_route(renderer_classes=[JSONRenderer,],methods=["POST",])
    def deactivate(self,request,pk):
        status = False;
        u = User.objects.filter(pk=pk)
        #set_trace()
        if None == request.user or len(u) == 0:
            data = {"status":bool(status)}
            return Response(data)
        myself = bool(request.user.id == int(pk)) or bool(u[0].is_superuser)
        if myself:
            status = False
        else:
            u[0].is_active = False
            u[0].save()
            status = True
        data = {"status":bool(status)}
        return Response(data)

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    permission_classes = (IsAdminUser,)

class UserListView(generics.ListAPIView):
    '''
    List all users in a simple template
    TODO: Add search
    TODO: Add user management functions to this -> send JSON changes to /api/users, may need to store id's in elements, or call users from AJAX to fill form
    '''
    queryset = User.objects.order_by('username')
    serializer_class = UserSerializer
    renderer_classes = (TemplateHTMLRenderer,)
    template_name = "users/list.html"

    permission_classes = (ImprovedDjangoModelPermissions,)


# class DriverUserListView(generics.ListAPIView):
#     '''
#     Convenience for getting Drivers
#     '''
#     queryset = User.objects.filter(groups__name="Driver").order_by('username')
#     serializer_class = UserSerializer
#     renderer_classes = (JSONRenderer,)
#     pagination_class = None
#     permission_classes = (ImprovedDjangoModelPermissions,)

#
# For DRF routes
#

def set_routes(router):
    router.register(r'user', UserViewSet, base_name="user")
    router.register(r'group', GroupViewSet, base_name="group")
    return
#
# Views foir various functions
#

def get_auth_token(request):
    u = request.user
    if u is None:
        return JsonResponse({})
    else:
        t = Token.objects.filter(user=u).first()
        if t is None:
            return JsonResponse({})
        else:
            return JsonResponse({'token': t.key })

# def let_me_drive(request):
#     u = request.user
#     if u is not None and (u.is_superuser):
#         if not u.groups.filter(name="Driver"):
#             g = Group.objects.get(name="Driver")
#             u.groups.add(g)
#         if not u.groups.filter(name="Admin"):
#             g = Group.objects.get(name="Admin")
#             u.groups.add(g)
#         u.save()
#         msg = ""
#         dd = DriverData.objects.filter(driver=u)
#         if len(dd) == 0:
#             d = DriverData.objects.create(driver=u)
#             msg += ", user1 {1} data0 {0}".format(d.id,u.id)
#         else:
#             msg +=" on {0}".format(dd[0].created)
#         return JsonResponse({'success': "Promoted to Driver" + msg })
#     else:
#         return JsonResponse({'error': "You may not promote this user to a driver." })


def initialize_user_permissions_and_groups(request):
    logger.debug("initialize_user_permissions_and_groups...")
    if request.user.is_superuser:
        # parses the permissions in users/permssions.py and creates if necessary
        msg = parse_app_permissions()
        return HttpResponse(msg)
    else:
        return HttpResponse("IP logged")
