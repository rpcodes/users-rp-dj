from rest_framework import permissions
from logging import warn, info, error
from django.contrib.auth.models import Permission, Group, User
from django.contrib.contenttypes.models import ContentType

"""
Django Rest Framework permissions
App permissions is a dictionary containing Group Name keys and
  value of dict corresponding to each app name and anothwer level of objects
  corresponding to model level permissions

  Select from up to four permissions
  ["add","change","delete","view"]
  [] - all four permissions
  None - same as not listed, no permissions
"""
APP_PERMISSIONS = {
    "Admin" : {
        "auth" : {
            "user" : [],
            "group" : ["add","change","view"]
        },
    },
    "Member" : {
        "auth" : {
          "user" : None, # TODO: Can we still have view if owner?
          "group" : ["view"]
        },
    },
}

def parse_app_permissions():
    msg = "Parsing app permission map..."
    info(msg)
    return_msg = msg;
    for group_name in APP_PERMISSIONS:
        group,created = Group.objects.get_or_create(name=group_name)
        msg = "...for group {0} ({1}, created: {2})".format(group_name, group.id, created)
        info(msg)
        return_msg += msg
        return_msg += parse_app_models(APP_PERMISSIONS[group_name], group)
    return return_msg

def parse_app_models(perm_map, group):
    all_four = ["add","change","view","delete"]
    for app_label in perm_map:
        info("Parsing app {0}".format(app_label))
        model_perm_map = perm_map[app_label]
        for model_key in model_perm_map:
            # get perms for that model, inside this app
            perm_list = model_perm_map[model_key]
            if None is perm_list:
                info("No permissions declared for {0} {1}"
                  .format(model_key, app_label))
                continue
            elif 0 == len(perm_list):
                # Use all 4 perms here
                perm_list = all_four;
            info("Looking for {0} of {1}".format(model_key, app_label))
            # TODO: This may fail, what is best to do if it does? It indicates bad Configuration in this file (should APP_PERMISSIONS be moved to Settings.py?)
            try:
                content_model_id = ContentType.objects.get(
                  app_label=app_label.lower(),
                  model=model_key.lower()).id
            except Exception as e:
                error("ERROR: No Content Type Matching {0} {1}: {2}"
                  .format(app_label, model_key, e))
                continue
            for i in range(len(perm_list)):
                action = perm_list[i]
                codename = "{0}_{1}".format(action,model_key.lower())
                try:
                    p, created = Permission.objects.get_or_create(
                      content_type_id=content_model_id,
                      codename=codename)
                    info("Permission found for app {0}, content id {1}, codename {2}, (created? {3})"
                      .format(app_label, content_model_id, codename, created))
                    # add to group
                    group.permissions.add(p)
                except Exception as e:
                    warn("SECURITY WARNING: COULD NOT ADD PERMISSION CODENAMED {0}: {1}".format(codename, e))
    group.save()
    return "Updated."

class ImprovedDjangoModelPermissions(permissions.DjangoModelPermissions):
    # Map methods into required permission codes.
    # Override this if you need to also provide 'view' permissions,
    # or if you want to provide custom permission codes.
    # Used alongside django rest framework
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.

    Note that the generic views will check the appropriate object level
     permissions, but if you're writing your own custom views, you'll need
      to make sure you check the object level permission checks yourself.
       You can do so by calling self.check_object_permissions(request, obj)
       from the view once you have the object instance.

    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `owner`
        return request.user.is_superuser or obj.owner == request.user


class BlacklistPermission(permissions.BasePermission):
    """
    Global permission check for blacklisted IPs.
    """

    def has_permission(self, request, view):
        ip_addr = request.META['REMOTE_ADDR']
        blacklisted = Blacklist.objects.filter(ip_addr=ip_addr).exists()
        return not blacklisted
