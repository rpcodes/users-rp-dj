# Allow template to filter by group in Django template
# https://docs.djangoproject.com/en/1.8/howto/custom-template-tags/#code-layout
# https://www.abidibo.net/blog/2014/05/22/check-if-user-belongs-group-django-templates/
# Usage: {% load has_group %} ... {% if request.user|has_group:"mygroup" %} <p>User belongs to my group ...
from django import template
register=template.Library()
@register.filter(name='has_group')
def has_group(user, group_name):
    # group = Group.objects.get(name=group_name)
    #return bool(group in user.groups.all())
    return user.groups.filter(name=group_name).exists()
